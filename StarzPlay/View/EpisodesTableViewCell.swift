//
//  EpisodesTableViewCell.swift
//  StarzPlay
//
//  Created by apple on 05/04/2024.
//

import UIKit

class EpisodesTableViewCell: UITableViewCell {
    
    // Outlets
    @IBOutlet weak var imgEpisode: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated) 
    }
    
    // Configure
    func configure(model: Episode) {
        titleLabel.text = model.name
        if model.stillPath == "" || model.stillPath == nil {
            imgEpisode.image = UIImage(named: "dummyEpisode")
            
        } else {
            let url = Constant.Urls.imageBaseUrl + (model.stillPath ?? "")
            imgEpisode.kf.setImage(with: URL(string: url))
        }
    }
}
