//
//  SeasonTableViewCell.swift
//  StarzPlay
//
//  Created by apple on 05/04/2024.
//

import UIKit

protocol SeasonsProtocol: AnyObject {
    func getSeasonId(seasonId: Int)
}

class SeasonTableViewCell: UITableViewCell {
    
    // Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    // Variables
    var model: MovieModel?
    weak var delegate: SeasonsProtocol?
    var lastSelectedIndexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCollectionView()
    }
    
    // Helpers
    func setupCollectionView() {
        collectionView.registerCell("SeasonCollectionViewCell")
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .clear
        
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension SeasonTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model?.seasons?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeasonCollectionViewCell", for: indexPath) as! SeasonCollectionViewCell
        
        if let data = model?.seasons?[indexPath.row] {
            cell.configure(model: data)
        }
        
        if indexPath.item == 0 && lastSelectedIndexPath == nil {
            collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .init())
            cell.isSelected = true
            lastSelectedIndexPath = indexPath
        } else if let lastSelectedIndexPath = lastSelectedIndexPath, lastSelectedIndexPath == indexPath {
            collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .init())
            cell.isSelected = true
        } else {
            collectionView.deselectItem(at: indexPath, animated: false)
            cell.isSelected = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let data = model?.seasons?[indexPath.row] {
            delegate?.getSeasonId(seasonId: data.seasonNumber ?? 1)
        }
        
        lastSelectedIndexPath = indexPath
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 3 , height: 80)
    }
}
