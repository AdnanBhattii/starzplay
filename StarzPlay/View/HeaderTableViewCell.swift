//
//  HeaderTableViewCell.swift
//  StarzPlay
//
//  Created by apple on 05/04/2024.
//

import UIKit
import Kingfisher

class HeaderTableViewCell: UITableViewCell {
    
    // Outlets
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var totalSeasonLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // Variables
    var btnPlayCallBack: (() -> ())?
    var btnTrailerCallBack: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    // Configure
    func configure(model: MovieModel?) {
        if model?.backdropPath == "" || model?.backdropPath == nil {
            imgHeader.image = UIImage(named: "header")
        } else {
            let url = Constant.Urls.imageBaseUrl + (model?.backdropPath ?? "")
            imgHeader.kf.setImage(with: URL(string: url))
        }
        
        titleLabel.text = model?.name
        totalSeasonLabel.text = "\(model?.numberOfSeasons ?? 0)" + " Season"
        descriptionLabel.text = model?.overview
        
        if let airDate = model?.firstAirDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            if let date = dateFormatter.date(from: airDate) {
                let year = Calendar.current.component(.year, from: date)
                yearLabel.text = "\(year)"
            }
        } else {
            yearLabel.text = "..."
        }
    }
    
    // Actions
    @IBAction func playPressed(_ sender: Any) {
        btnPlayCallBack?()
    }
    @IBAction func trailerPressed(_ sender: Any) {
        btnTrailerCallBack?()
    }
    
}
