//
//  SeasonCollectionViewCell.swift
//  StarzPlay
//
//  Created by apple on 05/04/2024.
//

import UIKit

class SeasonCollectionViewCell: UICollectionViewCell {
    
    // Outlets
    @IBOutlet weak var titleLabel: UILabel!
    
    // Variables
    override var isSelected: Bool {
           didSet {
               updateSelectionAppearance()
           }
       }
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        updateSelectionAppearance()
    }
    
    // Configure Cell
    func configure(model: Season) {
        titleLabel.text = model.name
    }
    
    // Update cell appearance based on selection
    private func updateSelectionAppearance() {
           if isSelected {
               titleLabel.textColor = UIColor.white
               titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
               
           } else {
               titleLabel.textColor = UIColor.darkGray
               titleLabel.font = UIFont.systemFont(ofSize: 16)
               
           }
       }
}
