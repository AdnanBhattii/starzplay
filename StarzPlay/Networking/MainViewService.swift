//
//  MainViewService.swift
//  StarzPlay
//
//  Created by apple on 06/04/2024.
//

import Foundation
import Alamofire

protocol MainViewService: KLNetworkManager {}

extension MainViewService {
    
    func getMovies(success: @escaping (MovieModel?) -> Void, failure: @escaping (ErrorModel?) -> Void) {
        
        serverRequestMethod(.get, Constant.Urls.movie,
                            encoding: URLEncoding.default,
                            headers: ["Content-Type": "application/json"],
                            successCallback: success,
                            failureCallback: failure)
    }
    
    func getSeasonEpisodes(seriesId: Int, seasonId: Int, success: @escaping (SeasonModel?) -> Void, failure: @escaping (ErrorModel?) -> Void) {
        let params = "/\(seriesId)/season/\(seasonId)?language=en-US"
        serverRequestMethod(.get, Constant.Urls.baseUrl + params,
                            encoding: URLEncoding.default,
                            headers: ["Content-Type": "application/json"],
                            successCallback: success,
                            failureCallback: failure)
    }
}
