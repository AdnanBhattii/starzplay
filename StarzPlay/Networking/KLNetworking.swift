//
//  KLNetworking.swift
//  StarzPlay
//
//  Created by apple on 06/04/2024.
//

import Foundation
import SystemConfiguration
import Alamofire
import AnyCodable


enum KLNetworkStatusResult { case success, error }
enum KLNetworkStatusCode: Int { case created = 201, success = 200, unAuthorized = 401, noResponse = 204 }

protocol KLNetworkManager {}

extension KLNetworkManager {
    
    //Server Call
    @discardableResult internal func serverRequestMethod<T: Codable, E: Codable>(_ method: HTTPMethod,
                                                                                 _ URLString: URLConvertible,
                                                                                 parameters: Parameters? = nil,
                                                                                 encoding: ParameterEncoding = URLEncoding.default,
                                                                                 headers: [String: String]? = nil,
                                                                                 keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy = .convertFromSnakeCase,
                                                                                 dateDecodingStrategy: JSONDecoder.DateDecodingStrategy = .deferredToDate,
                                                                                 successCallback: @escaping ((T?) -> Void),
                                                                                 failureCallback: @escaping ((E?) -> Void)) -> DataRequest? {
        
        //Internet check
        if Reachability.isConnectedToNetwork() == false {
            failureCallback(nil)
            showNoNetworkAlert()
            return nil
        }
        
        let defaultHeaders = getDefaultHeaders()
        let combinedHeaders = defaultHeaders.merging(headers ?? [:]) { $1 }
        
        let httpHeaders = HTTPHeaders(combinedHeaders)
        
        let dataRequest = AF.request(URLString, method: method, parameters: parameters, encoding: encoding, headers: httpHeaders).validate().responseData { response in
            
            if response.response?.statusCode == KLNetworkStatusCode.noResponse.rawValue {
                print("noResponse...")
                return
            }
            
            
            //Other Case
            guard let data = response.data else {
                print("Alamofire response: nil");
                return
            }
            
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = keyDecodingStrategy
            decoder.dateDecodingStrategy = dateDecodingStrategy
            
            //Dump
            let dictionary = try? decoder.decode([String: AnyDecodable].self, from: data)
            print("- API Response Dump: \(dictionary ?? [:])")
            
            switch response.result {
                
            case .success:
                //Decode Response
                do {
                    let item = try decoder.decode(T.self, from: data)
                    successCallback(item)
                }
                //Catch Exceptions
                catch DecodingError.dataCorrupted(let context) {
                    print("Data Corrupted:", context.debugDescription)
                    failureCallback(nil)
                } catch DecodingError.keyNotFound(let key, let context) {
                    print("Key '\(key)' not found")
                    print("Debug Description:", context.debugDescription)
                    failureCallback(nil)
                } catch DecodingError.valueNotFound(let value, let context) {
                    print("Value '\(value)' not found")
                    print("Debug Description:", context.debugDescription)
                    failureCallback(nil)
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("Type '\(type)' mismatch")
                    print("Debug Description:", context.debugDescription)
                    failureCallback(nil)
                } catch {
                    print("Error:", error)
                    failureCallback(nil)
                }
                
            case .failure(let error):
                let item = try? decoder.decode(E.self, from: data)
                failureCallback(item)
                print("- network error item: \(error))")
                let dictionary = try? decoder.decode([String: AnyDecodable].self, from: data)
                print("- network error dic: \(dictionary ?? [:])")
                
            }
        }
        
        print(dataRequest)
        return dataRequest
        
        
    }
    
    //Headers
    func getDefaultHeaders() -> [String: String] {
        var headers = [
            "Content-Type"          : "application/x-www-form-urlencoded",
        ]
        
        let token = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI4MDMwMGM2MWM4YTM5YzZkNDdjNzM4YWZkZjI0MGVkYyIsInN1YiI6IjY2MGRjYWViMzU4MThmMDE3YzNjN2VhZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.U4OHEX_lodxzDGKgsNzovmQIo3pjAjEsOlgPgFZEYe4"
        headers["Authorization"] = "bearer \(token)"
        
        return headers
    }
    
    
    //No internet check
    func showNoNetworkAlert() {
        
        DispatchQueue.main.async {
            
        }
    }
    
}

//Reachability
public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}

private let arrayParametersKey = "arrayParametersKey"

/// Extenstion that allows an array be sent as a request parameters
extension Array {
    /// Convert the receiver array to a `Parameters` object.
    func asParameters() -> Parameters {
        return [arrayParametersKey: self]
    }
}

/// Convert the parameters into a json array, and it is added as the request body.
/// The array must be sent as parameters using its `asParameters` method.
public struct ArrayEncoding: ParameterEncoding {
    
    /// The options for writing the parameters as JSON data.
    public let options: JSONSerialization.WritingOptions
    
    
    /// Creates a new instance of the encoding using the given options
    ///
    /// - parameter options: The options used to encode the json. Default is `[]`
    ///
    /// - returns: The new instance
    public init(options: JSONSerialization.WritingOptions = []) {
        self.options = options
    }
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var urlRequest = try urlRequest.asURLRequest()
        
        guard let parameters = parameters,
              let array = parameters[arrayParametersKey] else {
            return urlRequest
        }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: array, options: options)
            
            if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }
            
            urlRequest.httpBody = data
            
        } catch {
            throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
        }
        
        return urlRequest
    }
}

