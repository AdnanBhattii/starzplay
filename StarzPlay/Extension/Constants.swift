//
//  Constants.swift
//  StarzPlay
//
//  Created by apple on 05/04/2024.
//

import Foundation
import UIKit

struct Constant {
    
    struct Urls {
        static let baseUrl = "https://api.themoviedb.org/3/tv/"
        static let imageBaseUrl = "https://image.tmdb.org/t/p/w500/"
        
        static let movie = baseUrl + "62852?api_key=3d0cda4466f269e793e9283f6ce0b75e"
        
        static let seriesId = 62852
        static let videoPlayerURL = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4"
    }
}
