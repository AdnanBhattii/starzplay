//
//  UITableView+Common.swift
//  StarzPlay
//
//  Created by apple on 05/04/2024.
//

import Foundation
import UIKit

extension UITableView {
    
    func registerCell(_ idAndNibName: String) {
        let nib = UINib(nibName: idAndNibName, bundle: .main)
        register(nib, forCellReuseIdentifier: idAndNibName)
    }
}



