//
//  UIView+Layer.swift
//  StarzPlay
//
//  Created by apple on 05/04/2024.
//

import Foundation
import UIKit

extension UIView {
    
    func dropShadow(color: UIColor = .gray, opacity: Float = 0.4, offSet: CGSize = CGSize.zero, radius: CGFloat = 1, pathRadius: CGFloat? = nil, scale: Bool = true) {
        
        DispatchQueue.main.async {
            
            self.layer.masksToBounds = false
            self.layer.shadowColor = color.cgColor
            self.layer.shadowOpacity = opacity
            self.layer.shadowOffset = offSet
            self.layer.shadowRadius = radius
            
            
            var shadowPathRef: CGPath?
            
            if pathRadius != nil {
                shadowPathRef = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: /*2.0 * pathRadius!*/self.frame.width, height: 2.0 * pathRadius!), cornerRadius: pathRadius!).cgPath
            }
            else {
                shadowPathRef = UIBezierPath(rect: self.bounds).cgPath
            }
            
            self.layer.shadowPath = shadowPathRef
            self.layer.shouldRasterize = true
            self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
            
        }
    }
    
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func removeShadow() {
        self.layer.shadowColor = UIColor.clear.cgColor
        self.layer.shadowOpacity = 0
        self.layer.shadowRadius = 0
        self.layer.shadowPath = nil
    }
    
    @IBInspectable var showdropShadow: Bool {
        get {
            return self.layer.shadowColor == UIColor.clear.cgColor ? false : true
        }
        set {
            if newValue {
                dropShadow(radius: 0.2)
            } else {
                removeShadow()
            }
        }
    }
    
    //Border
    @IBInspectable var borderColor: UIColor? {
        get {
            return layer.borderColor.map(UIColor.init)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    func roundCircleView() {
        DispatchQueue.main.async {
            self.cornerRadius = self.frame.size.height * 0.5
            self.clipsToBounds = true
            self.setNeedsLayout()
        }
    }
    
    
    func roundCornersView(corners: UIRectCorner, radius: CGFloat) {
        if #available(iOS 11.0, *) {
            let cornerMasks = [
                corners.contains(.topLeft) ? CACornerMask.layerMinXMinYCorner : nil,
                corners.contains(.topRight) ? CACornerMask.layerMaxXMinYCorner : nil,
                corners.contains(.bottomLeft) ? CACornerMask.layerMinXMaxYCorner : nil,
                corners.contains(.bottomRight) ? CACornerMask.layerMaxXMaxYCorner : nil,
                corners.contains(.allCorners) ? [CACornerMask.layerMinXMinYCorner, CACornerMask.layerMaxXMinYCorner, CACornerMask.layerMinXMaxYCorner, CACornerMask.layerMaxXMaxYCorner] : nil
            ].compactMap({ $0 })
            
            var maskedCorners: CACornerMask = []
            cornerMasks.forEach { (mask) in maskedCorners.insert(mask) }
            
            self.clipsToBounds = true
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = maskedCorners
        } else {
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
        }
    }
}

extension UIView {
    func makeCornerTabBar(shadowColor: UIColor = UIColor.black,
                          fillColor: UIColor = UIColor.white,
                          opacity: Float = 0.2,
                          corners: UIRectCorner,
                          offset: CGSize = CGSize(width: 0.0, height: 5.0),
                          radius: CGFloat = 0.0) -> CAShapeLayer {
        
        let shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
        shadowLayer.fillColor = fillColor.cgColor
        shadowLayer.shadowColor = shadowColor.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = offset
        shadowLayer.shadowOpacity = opacity
        layer.mask = shadowLayer
        layer.insertSublayer(shadowLayer, at: 0)
        return shadowLayer
    }
}



