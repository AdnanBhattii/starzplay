//
//  VideoPlayerViewController.swift
//  StarzPlay
//
//  Created by apple on 06/04/2024.
//

import UIKit
import AVFoundation
import AVKit

class VideoPlayerViewController: UIViewController {

    // Outlets
    @IBOutlet weak var playerView: UIView!
    
    // Variables
    var player: AVPlayer?
    var playerViewController: AVPlayerViewController?
    var playerURL: String = ""
    
    
    // Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPlayer()
    }

    // Helpers
    func setupPlayer() {
        guard let videoURL = URL(string: playerURL) else {
            return
        }

        player = AVPlayer(url: videoURL)
        playerViewController = AVPlayerViewController()
        playerViewController?.player = player

        addChild(playerViewController!)
        playerView.addSubview(playerViewController!.view)
        playerViewController?.view.frame = playerView.bounds

        player?.play()
    }

    // Actions
    @IBAction func backPressed(_ sender: Any) {
        player?.pause()
        navigationController?.popViewController(animated: true)
    }
}
