//
//  MainViewController.swift
//  StarzPlay
//
//  Created by apple on 05/04/2024.
//

import UIKit
import Kingfisher

class MainViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // Variable
    var viewModel = MainViewModel()
    
    // Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getMoviesApi()
        getSeasonApi(seriesId: Constant.Urls.seriesId, seasonNumber: 1)
        setupTableView()
    }
    
    // Helpers
    func setupTableView() {
        tableView.registerCell("HeaderTableViewCell")
        tableView.registerCell("SeasonTableViewCell")
        tableView.registerCell("EpisodesTableViewCell")
        
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        
        tableView.backgroundColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        
    }
}

// MARK: UITableView
extension MainViewController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else if section == 1 {
            return 1
        }
        else {
            return viewModel.seasaonModel?.episodes?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        if section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell", for: indexPath) as! HeaderTableViewCell
            if let data = viewModel.model {
                cell.configure(model: data)
            }
            
            cell.btnPlayCallBack = {
                let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerViewController") as! VideoPlayerViewController
                videoPlayerVC.playerURL = Constant.Urls.videoPlayerURL
                self.navigationController?.pushViewController(videoPlayerVC, animated: true)
                
            }
            cell.btnTrailerCallBack = {
                let videoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "VideoPlayerViewController") as! VideoPlayerViewController
                videoPlayerVC.playerURL = Constant.Urls.videoPlayerURL
                self.navigationController?.pushViewController(videoPlayerVC, animated: true)
            }
            cell.selectionStyle = .none
            return cell
        }
        
        else if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SeasonTableViewCell", for: indexPath) as! SeasonTableViewCell
            cell.selectionStyle = .none
            cell.model = viewModel.model
            cell.delegate = self
            cell.collectionView.reloadData()
            
            return cell
        }
        
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EpisodesTableViewCell", for: indexPath) as! EpisodesTableViewCell
            if let data = viewModel.seasaonModel?.episodes?[indexPath.row] {
                cell.configure(model: data)
            }
            
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = indexPath.section
        if section == 0 {
            return UITableView.automaticDimension
        }
        if section == 1 {
            return 60
        }
        else {
            return UITableView.automaticDimension
        }
    }
}



// MARK: Delegate
extension MainViewController: SeasonsProtocol {
    func getSeasonId(seasonId: Int) {
        print("SeasonID: \(seasonId)")
        getSeasonApi(seriesId: Constant.Urls.seriesId, seasonNumber: seasonId)
    }
}

// MARK: Calling Api
extension MainViewController {
    // Movies Api
    func getMoviesApi() {
        viewModel.fetchMovies { status in
            if status == .success {
                print("Success api.")
                self.tableView.reloadData()
            } else {
                print("Failure")
            }
        }
    }
    
    // Get Episodes
    func getSeasonApi(seriesId: Int, seasonNumber: Int) { // Seasons Episode Api
        viewModel.fetchSeasonEpisode(seriesId: seriesId, seasonNumber: seasonNumber) { status in
            if status == .success {
                print("Succes")
                DispatchQueue.main.async {
                    self.tableView.reloadSections(IndexSet(integer: 2), with: .automatic)
                }
            } else {
                print("Failure")
            }
        }
    }
}
