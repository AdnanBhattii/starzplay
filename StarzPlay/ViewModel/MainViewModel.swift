//
//  MainViewModel.swift
//  StarzPlay
//
//  Created by apple on 06/04/2024.
//

import Foundation

class MainViewModel: MainViewService {
    
    // Variables
    var model: MovieModel?
    var seasaonModel: SeasonModel?
    var errorModel: ErrorModel?
    
    // Movies Api
    func fetchMovies(completion: @escaping (KLNetworkStatusResult) -> Void) {
        
        getMovies(success: { (model) in
            print("response")
            self.model = model
            completion(.success)
            print(model.debugDescription)
            
        }) { ErrorModel in
            self.errorModel = ErrorModel
            completion(.error)
            print("error ")
            print(ErrorModel.debugDescription)
        }
    }
    
    // Fetch Season Episode Api
    func fetchSeasonEpisode(seriesId: Int, seasonNumber: Int,  completion: @escaping (KLNetworkStatusResult) -> Void) {
        
        getSeasonEpisodes(seriesId: seriesId, seasonId: seasonNumber, success: { (model) in
            print("response")
            self.seasaonModel = model
            completion(.success)
            print(model.debugDescription)
            
        }) { ErrorModel in
            self.errorModel = ErrorModel
            completion(.error)
            print("error ")
            print(ErrorModel.debugDescription)
        }
    }
}
