//
//  SeasonModel.swift
//  StarzPlay
//
//  Created by apple on 06/04/2024.
//

import Foundation

struct SeasonModel: Codable {
    let episodes: [Episode]?
    let name, overview: String?
    let welcomeID: Int?
    let posterPath: String?
    let seasonNumber: Int?
    let voteAverage: Double?
    
}

struct Episode: Codable {
    let airDate: String?
    let episodeNumber: Int?
    let id: Int?
    let name, overview, productionCode: String?
    let runtime, seasonNumber, showID: Int?
    let stillPath: String?
    let voteAverage: Double?
    let voteCount: Int?
}

struct Crew: Codable {
    let job: String?
    let creditID: String?
    let adult: Bool?
    let gender, id: Int?
    let name, originalName: String?
    let popularity: Double?
    let profilePath: String?
    let character: String?
    let order: Int?
    
}
