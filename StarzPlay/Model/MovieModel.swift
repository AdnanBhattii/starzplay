//
//  MovieModel.swift
//  StarzPlay
//
//  Created by apple on 06/04/2024.
//

import Foundation

struct MovieModel: Codable {
    let adult: Bool?
    let backdropPath: String?
    let created_By: [CreatedBy]?
    let episodeRunTime: [Int]?
    let firstAirDate: String?
    let genres: [Genre]?
    let homepage: String?
    let id: Int?
    let inProduction: Bool?
    let languages: [String]?
    let lastAirDate: String?
    let lastEpisodeToAir: LastEpisodeToAir?
    let name: String?
    let nextEpisodeToAir: String?
    let networks: [Network]?
    let numberOfEpisodes, numberOfSeasons: Int?
    let originCountry: [String]?
    let originalLanguage, originalName, overview: String?
    let popularity: Double?
    let posterPath: String?
    let productionCompanies: [Network]?
    let productionCountries: [ProductionCountry]?
    let seasons: [Season]?
    let spokenLanguages: [SpokenLanguage]?
    let status, tagline, type: String?
    let voteAverage: Double?
    let voteCount: Int?

}

struct CreatedBy: Codable {
    let id: Int?
    let creditID, name: String?
    let gender: Int?
    let profilePath: String?

}

struct Genre: Codable {
    let id: Int?
    let name: String?
}

struct LastEpisodeToAir: Codable {
    let id: Int?
    let name, overview: String?
    let voteAverage, voteCount: Int?
    let airDate: String?
    let episodeNumber: Int?
    let episodeType, productionCode: String?
    let runtime, seasonNumber, showID: Int?
    let stillPath: String?

}

struct Network: Codable {
    let id: Int?
    let logoPath: String?
    let name, originCountry: String?

}

struct ProductionCountry: Codable {
    let iso3166_1, name: String?

}

struct Season: Codable {
    let airDate: String?
    let episodeCount, id: Int?
    let name, overview, poster_path: String?
    let seasonNumber: Int?
    let voteAverage: Double?

}

struct SpokenLanguage: Codable {
    let iso639_1: String?
    let name: String?
}
