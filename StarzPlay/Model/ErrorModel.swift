//
//  ErrorModel.swift
//  StarzPlay
//
//  Created by apple on 06/04/2024.
//

import Foundation

struct ErrorModel: Codable {
    var message: String?
    var error: String?
    var errorDescription: String?
    var title: String?
    var Detail: String?
}
